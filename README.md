# MaxAM et entropie

Ce dépot s'intéresse à la technique d'échantillonnage `MaxAM` décrite dans l'article publié en 2020 et intitulé [Active learning based on belief functions](https://link.springer.com/article/10.1007/s11432-020-3082-9) de S. Zhang, D. Han et Y. Yang. L'article présente, à mon sens, quelques imprécisions qui n'aident pas à l'implémentation de la proposition ; cela explique peut-être les performances obtenues...

On s'intéresse aussi aux différentes entropies possibles telles que décrites en 2018 dans [A new definition of entropy of belief functions in the Dempster-Shafer theory](https://www.sciencedirect.com/science/article/pii/S0888613X17300786) de R. Jiroušek et P. P. Shenoy.
