#
#
#
#
#   Maximum Ambiguity Measure
#   (also called Uncertainty sampling based on belief function)
#
#   See https://doi.org/10.1007/s11432-020-3082-9 for more details:
#
#       Zhang S X, Han D Q, Yang Y. Active learning based on belief
#       functions. Sci China Inf Sci, 2020, 63(11): 210205
#
#
#

import math
import numpy as np

from collections import deque

from experiment.ibelief import mtobetp


# -----------------------------------------------------------------------------
#   Ambiguity Measure
# -----------------------------------------------------------------------------

def ambiguity_measure(m):
    """
    Compute the ambiguity measure.

    Parameters
    ----------
    m : ndarray of shape (n_samples, n_classes)
        Basic probability assignments associated to each class of the
        frame of discernment (i. e. the classes or target values).
        First column is assigned to the empty set; last column to the
        whole frame of discernment.
    
    Returns
    -------
    ndarray of shape (n_samples,)
        The ambiguity measure of each sample.
    """
    ambiguity = []
    
    for i in range(m.shape[0]):
        pignistic_probability = mtobetp(m[i])
        ambiguity.append(
            -np.sum(pignistic_probability * np.log2(pignistic_probability))
        )
    
    return np.array(ambiguity)


# -----------------------------------------------------------------------------
#   Maximum Ambiguity Measure
# -----------------------------------------------------------------------------

def max_am(learner, X, bpa_threshold=.5, n_instances=1):
    """
    Choose the samples which maximise the ambiguity measure.
    
    Parameters
    ----------
    learner : instance of modAL.models.ActiveLearner
        The learner to use.
    
    X : ndarray of shape (n_samples, n_features)
        The samples.
    
    bpa_threshold : float
        The maximum value for the bpa of the empty set. If this
        threshold is passed, then the sample is discarded as it might
        be an outlier. Noted as letter eta in the paper.

    n_instances : int
        The number of samples to sample.
    
    Returns
    -------
    query_idx : ndarray of shape (n_instances,)
        The indices of the selected samples.
    
    query_samples : ndarray of shape (n_instances, n_features)
        The selected samples.
    """

    if X.shape[0] < n_instances:
        n_instances = X.shape[0]
    
    query_idx = []

    X_labeled   = learner.X_training
    X_unlabeled = X

    bpa = _compute_bpa(X_unlabeled, X_labeled, learner.predict_proba)
    bpa_empty_set, bpa_frame_of_discernment =   _compute_other_bpa(
                                                    X_unlabeled,
                                                    X_labeled,
                                                    bpa
                                                )
    
    bpa_matrix = _bpa_to_matrix(bpa, bpa_empty_set, bpa_frame_of_discernment)
    candidates = []

    for i in range(X.shape[0]):
        if bpa_matrix[i, 0] < bpa_threshold:
            candidates.append(i)

    am = ambiguity_measure(bpa_matrix[candidates])

    # print(f"\n{'Id':5} {'X[id]':12} {'AM':18} {'m(/)':20}")
    # for i in range(len(candidates)):
    #     print(f"{candidates[i]:5} "
    #           f"({X[candidates][i, 0]:3}, {X[candidates][i, 1]:3}) "
    #           f"{am[i]:20} "
    #           f"{bpa_matrix[candidates][i, 0]}")
    
    # Select the n_instances best samples
    for _ in range(n_instances):
        max_am_value = -1
        max_am_index = -1

        for i in range(len(candidates)):
            am_value, am_index = am[i], candidates[i]
            
            # TODO: To improve!
            if am_value > max_am_value and am_index not in query_idx:
                max_am_value = am_value
                max_am_index = am_index
        
        query_idx.append(max_am_index)
        
    # print("\nResult:")
    # print(query_idx)

    return np.array(query_idx), X[query_idx]


# -----------------------------------------------------------------------------
#   Internal function: _bpa_to_matrix
# -----------------------------------------------------------------------------

def _bpa_to_matrix(bpa_classes, bpa_empty_set, bpa_frame_of_discernment):
    """
    Return a matrix containing the mass functions.
    
    Parameters
    ----------
    bpa_classes : ndarray of shape (n_samples, n_classes)
        The mass functions on the classes.
    
    bpa_empty_set : ndarray of shape (n_samples,)
        The mass functions on the empty set.
    
    bpa_frame_of_discernment : ndarray of shape (n_samples,)
        The mass functions on the frame of discernment.
    
    Returns
    -------
    m : ndarray of shape (n_samples, 2^n_classes)
        The mass functions on everything above.
    """
    m = np.zeros((bpa_classes.shape[0], 2**bpa_classes.shape[1]))

    for i in range(bpa_classes.shape[1]):
        m[:, 2**i] = bpa_classes[:, i]
    
    m[:, 0], m[:, -1] = bpa_empty_set, bpa_frame_of_discernment
    
    return m


# -----------------------------------------------------------------------------
#   Internal function: _compute_bpa
# -----------------------------------------------------------------------------

def _compute_bpa(X_unlabeled, X_labeled, p, a=1.):
    """
    Compute the basic probability assignment in the way described in the
    article https://doi.org/10.1007/s11432-020-3082-9.

    Parameters
    ----------
    X_unlabeled : ndarray of shape (n_samples, n_features)
        The unlabeled samples.
    
    X_labeled : ndarray of shape (n_samples', n_features')
        The labeled samples.

    p : callable
        The function which computes the probabilities.
    
    a : float
        A parameter between 0 and 1 dictating the discounting
        coefficient. Noted as letter alpha in the paper.

    Returns
    -------
    ndarray of shape (n_samples, n_classes)
        The basic probability assignments associated to the samples and
        their classes.
    """
    A = np.exp(-a * _distances_among_labeled(X_unlabeled, X_labeled))
    B = p(X_unlabeled)
    
    return A.reshape(-1, 1) * B


# -----------------------------------------------------------------------------
#   Internal function: _compute_other_bpa
# -----------------------------------------------------------------------------

def _compute_other_bpa(X_unlabeled, X_labeled, samples_bpa, b=1.):
    """
    Compute the basic probability assignments of the empty set and the 
    frame of discernment.

    Parameters
    ----------
    X_unlabeled : ndarray of shape (n_samples, n_features)
        The unlabeled samples.
    
    X_labeled : ndarray of shape (n_samples', n_features')
        The labeled samples.
    
    samples_bpa : ndarray of shape (n_samples, n_classes)
        The basic probability assignments associated to the samples and
        their classes.
    
    b : float
        A parameter between 0 and 1 dictating the discounting
        coefficient. Noted as letter beta in the paper.
    
    Returns
    -------
    bpa_empty_set : ndarray of shape (n_samples,)
        The basic probability assignment affected to the empty set.
    
    bpa_frame_discernment : ndarray of shape (n_samples,)
        The basic probability assignment affected to the whole frame of
        discernment.
    """
    bpa_sum = np.sum(samples_bpa, axis=1)

    bpa_empty_set = (
        np.exp(-b * _distances_among_all(X_unlabeled, X_labeled))
        * (1 - bpa_sum)
    )

    bpa_frame_of_discernment = 1 - bpa_sum - bpa_empty_set

    return bpa_empty_set, bpa_frame_of_discernment


# -----------------------------------------------------------------------------
#   Internal function: _distances_among_all
# -----------------------------------------------------------------------------

def _distances_among_all(X_unlabeled, X_labeled):
    """
    For each unlabeled sample, compute the euclidian distance to the
    closest labeled OR unlabeled sample.

    Parameters
    ----------
    X_unlabeled : ndarray of shape (n_samples, n_features)
        The unlabeled samples.
    
    X_labeled : ndarray of shape (n_samples', n_features')
        The labeled samples.

    Returns
    -------
    distances : ndarray of shape (n_samples,)
        The distance between each sample and the closest labeled OR
        unlabeled sample.
    """
    distances = np.zeros(X_unlabeled.shape[0])
    distances.fill(math.inf)

    for i in range(X_unlabeled.shape[0]):
        a = X_unlabeled[i]

        # Look for the closest sample among the labeled
        for b in X_labeled:
            distance_a_b = np.sqrt(np.sum(np.square(a - b)))

            if distance_a_b < distances[i]:
                distances[i] = distance_a_b
        
        # Look for the closest sample among the unlabeled
        for b in X_unlabeled:
            distance_a_b = np.sqrt(np.sum(np.square(a - b)))

            # "0 <" prevents from computing the distance between the
            # same sample (which would be 0).
            if 0 < distance_a_b < distances[i]:
                distances[i] = distance_a_b

    # Only the distances of the closest samples among the labeled and
    # unlabeled samples are returned.
    return distances


# -----------------------------------------------------------------------------
#   Internal function: _distances_among_labeled
# -----------------------------------------------------------------------------

def _distances_among_labeled(X_unlabeled, X_labeled):
    """
    For each unlabeled sample, compute the euclidian distance to the
    closest labeled sample.

    Parameters
    ----------
    X_unlabeled : ndarray of shape (n_samples, n_features)
        The unlabeled samples.
    
    X_labeled : ndarray of shape (n_samples', n_features')
        The labeled samples.

    Returns
    -------
    distances : ndarray of shape (n_samples,)
        The distance between each sample and the closest labeled sample.
    """
    distances = np.zeros(X_unlabeled.shape[0])
    distances.fill(math.inf)

    for i in range(X_unlabeled.shape[0]):
        a = X_unlabeled[i]

        for j in range(X_labeled.shape[0]):
            b = X_labeled[j]
            distance_a_b = np.sqrt(np.sum(np.square(a - b)))

            if distance_a_b < distances[i]:
                distances[i] = distance_a_b

    return distances

