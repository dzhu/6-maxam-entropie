#
#
#
#
#   Local library.
#
#
#
#

import time

import numpy as np

import matplotlib.pyplot as plt

from collections import Counter

from scipy import sparse, stats

from .eknn_sampling import EKNNSampling

from sklearn import preprocessing, model_selection, metrics, compose

from modAL import ActiveLearner


# -----------------------------------------------------------------------------
#   bootstrap
# -----------------------------------------------------------------------------

def bootstrap(X, y, indices, samples_per_class=5):
    """
    Select a certain number of samples of each class.

    Parameters
    ----------
    X : ndarray of shape (n_samples, n_features)
        The samples.
    
    y : ndarray of shape (n_samples,)
        The target values.

    indices : ndarray of shape (n_samples,)
        The indices of the samples from which the representants of each class will be drawn.
    
    samples_per_class : int
        Number of representant for each class.
    
    Returns
    -------
    bootset : list
        The samples' indices of the bootstrap.
    """

    counter = dict()
    bootset = set()

    for i in range(len(indices)):
        index  = indices[i]
        update = False

        if y[index] not in counter:
            counter[y[index]] = 1
            update = True
        elif counter[y[index]] < samples_per_class:
            counter[y[index]] += 1
            update = True
        else:
            pass
        
        # The indices have been updated
        if update:
            bootset.add(index)
            indices[i] = None
    
    return _remove_from_iterable(indices), list(bootset)


# -----------------------------------------------------------------------------
#   compare_scores
# -----------------------------------------------------------------------------

def compare_scores(scores_1, scores_2):
    """
    Compare scores of scores_1 over scores_2. The comparison uses a paired t-test on each score from both lists.

    Parameters
    ----------
    scores_1 : list of list
        A list of records of different metrics.
    
    scores_2 : list of list
        Anoter list of records of different metrics.
    
    Returns
    -------
    results : list
        The result for each comparison between the two inputs. If value is 0, then it's a tie, if value is 1, then it's
        a Win for scores_1 over scores_2, and if value is -1, then it's a Loss for scores_1 over scores_2.
    """
    
    if len(scores_1) != len(scores_2):
        raise ValueError("Lists scores_1 and scores_2 must have the same length")
    
    results = [0 for _ in range(len(scores_1))]

    for i in range(len(scores_1)):
        tstats, pvalue = stats.ttest_rel(scores_1[i], scores_2[i])

        # print(f"t-statistic: {round(tstats, 5):>10} || " \
        #       f"p-value: {round(pvalue, 5):>10} || " \
        #       f"p-value < 5 % {pvalue <= 0.05}")
        
        # Statistically significant
        if pvalue <= 0.05:
            mean_1 = sum(scores_1[i]) / len(scores_1[i])
            mean_2 = sum(scores_2[i]) / len(scores_2[i])

            if mean_1 > mean_2:
                results[i] = 1
            elif mean_1 < mean_2:
                results[i] = -1
            else:
                pass
        
        # Not statistically significant
        else:
            pass
    
    return results


# -----------------------------------------------------------------------------
#   experiment_real
# -----------------------------------------------------------------------------

def experiment_real(
        alias,
        X,
        y,
        estimator,
        query_strategies,
        n_experiments = 5,
        n_queries     = 200,
        n_samples_per_iteration = 10,
        average       = "macro",
        multi_class   = "ovr",
        test_size     = .5,
        method        = 0,
        **kwargs
    ):
    """
    Launch experiments on real dataset and compare results when the query strategy is changed.

    Parameters
    ----------
    alias : string
        A name for the test.
    
    X : ndarray of shape (n_samples, n_features)
        The samples.
    
    y : ndarray of shape (n_samples,)
        The target values.
    
    estimator : callable
        The classifier to use.
    
    query_strategies : tuple of 2 callables
        Query strategy to use. The comparison follow the order of the given query strategies. Write "None" for the
        random sampling.
    
    n_experiments : int
        The experiment will be repeated n_experiments times.
    
    n_queries : int
        Maximum number of labels provided.
    
    n_samples_per_iteration : int
        Number of samples to query for each iteration.
    
    average : string
        Must be "macro", "micro", "weighted"...
    
    multi_class : string
        If specified, must be either "ovr" (One vs Rest) or "ovo" (One vs One).
    
    test_size : float
        Proportion of the test set.
    
    method : int or string
        See `criterion` argument from `ibelief.entropy()`.

    **kwargs : dict
        Parameters for the estimator.
    """
    
    n_metrics = 1 # one metric
    
    scores_1 = np.empty((n_experiments, n_metrics*(n_queries//n_samples_per_iteration)))
    scores_2 = np.empty((n_experiments, n_metrics*(n_queries//n_samples_per_iteration)))

    if n_queries > X.shape[0]*(1-test_size):
        raise ValueError(f"Too few data to train")

    for i in range(n_experiments):
        experiment_score_1 = []
        experiment_score_2 = []
        
        # Split data
        train, test = train_test_indices(X, y, test_size=test_size)
        unlabeled, labeled = bootstrap(X, y, train, 5)

        # First sampling strategy
        learn(
            X         = X, 
            y         = y, 
            unlabeled = unlabeled.copy(),
            labeled   = labeled.copy(),
            test      = test.copy(),
            scores    = experiment_score_1,
            estimator = estimator,
            strategy  = query_strategies[0],
            n_queries = n_queries,
            n_samples_per_iteration = n_samples_per_iteration,
            average   = average,
            multi_class = multi_class,

            # Estimators' arguments
            **kwargs
        )
        
        # Second sampling strategy
        learn(
            X         = X, 
            y         = y, 
            unlabeled = unlabeled.copy(),
            labeled   = labeled.copy(),
            test      = test.copy(),
            scores    = experiment_score_2,
            estimator = estimator,
            strategy  = query_strategies[1],
            n_queries = n_queries,
            n_samples_per_iteration = n_samples_per_iteration,
            average   = average,
            multi_class = multi_class,

            # Estimators' arguments
            **kwargs
        )

        scores_1[i] = experiment_score_1
        scores_2[i] = experiment_score_2
    
    with open(f"matrix/{alias}.npy", "wb") as file:
        np.save(file, scores_1)
        np.save(file, scores_2)
    
    scores_1 = retrieve_scores(scores_1, n_metrics, n_queries//n_samples_per_iteration)
    scores_2 = retrieve_scores(scores_2, n_metrics, n_queries//n_samples_per_iteration)

    return scores_1[0], scores_2[0]


# -----------------------------------------------------------------------------
#   experiment_syn
# -----------------------------------------------------------------------------

def experiment_syn(
        alias,
        X,
        y,
        estimator,
        query_strategies,
        n_experiments = 10,
        n_queries     = 200,
        n_samples_per_iteration = 10,
        average       = "macro",
        multi_class   = None,
        test_size     = .0909, # 9,09 % of 11000 = 1000, therefore 10000 in training set and 1000 in test set
        method        = 0,
        **kwargs
    ):
    """
    Launch experiments on synthetic binary dataset and compare results when the query strategy is changed.

    Parameters
    ----------
    alias : string
        A name for the test.
    
    X : ndarray of shape (n_samples, n_features)
        The samples.
    
    y : ndarray of shape (n_samples,)
        The target values.
    
    estimator : callable
        The classifier to use.
    
    query_strategies : tuple of 2 callables
        Query strategy to use. The comparison follow the order of the given query strategies. Write "None" for the
        random sampling.
    
    n_experiments : int
        The experiment will be repeated n_experiments times.
    
    n_queries : int
        Maximum number of labels provided.
    
    n_samples_per_iteration : int
        Number of samples to query for each iteration.
    
    average : string
        Must be "macro", "micro", "weighted"...
    
    multi_class : string
        If specified, must be either "ovr" (One vs Rest) or "ovo" (One vs One).
    
    test_size : float
        Proportion of the test set.
    
    method : int or string
        See `criterion` argument from `ibelief.entropy()`.

    **kwargs : dict
        Parameters for the estimator.
    """
    
    n_metrics = 1 # one metric
    
    scores_1 = np.empty((n_experiments, n_metrics*(n_queries//n_samples_per_iteration)))
    scores_2 = np.empty((n_experiments, n_metrics*(n_queries//n_samples_per_iteration)))

    for i in range(n_experiments):
        
        experiment_score_1 = []
        experiment_score_2 = []
        
        # Split data
        train, test = train_test_indices(X, y, test_size=test_size)
        unlabeled, labeled = bootstrap(X, y, train, 5)

        # First sampling strategy
        learn(
            X         = X, 
            y         = y, 
            unlabeled = unlabeled.copy(),
            labeled   = labeled.copy(),
            test      = test.copy(),
            scores    = experiment_score_1,
            estimator = estimator,
            strategy  = query_strategies[0],
            n_queries = n_queries,
            n_samples_per_iteration = n_samples_per_iteration,
            average   = average,
            multi_class = multi_class,

            # Estimators' arguments
            **kwargs
        )
        
        # Second sampling strategy
        learn(
            X         = X, 
            y         = y, 
            unlabeled = unlabeled.copy(),
            labeled   = labeled.copy(),
            test      = test.copy(),
            scores    = experiment_score_2,
            estimator = estimator,
            strategy  = query_strategies[1],
            n_queries = n_queries,
            n_samples_per_iteration = n_samples_per_iteration,
            average   = average,
            multi_class = multi_class,

            # Estimators' arguments
            **kwargs
        )

        scores_1[i] = experiment_score_1
        scores_2[i] = experiment_score_2
    
    with open(f"matrix/{alias}.npy", "wb") as file:
        np.save(file, scores_1)
        np.save(file, scores_2)
    
    scores_1 = retrieve_scores(scores_1, n_metrics, n_queries//n_samples_per_iteration)
    scores_2 = retrieve_scores(scores_2, n_metrics, n_queries//n_samples_per_iteration)

    return scores_1[0], scores_2[0]


# -----------------------------------------------------------------------------
#   get_X_y
# -----------------------------------------------------------------------------

def get_X_y(filepath, target_position=-1, drop=[], features_to_transform=[], \
            sep=',', dtype=np.float16, return_transformers=False):
    """
    Load a CSV file and return samples and target values.

    Parameters
    ----------
    filepath : string
        Path to the CSV file.
    
    target_position : int
        Position of the target values.
    
    drop : list
        List of columns that won't be kept.
    
    features_to_transform : list
        Must be a list whose elements are: (transformation_name, transformation_type, columns_indices).
        For example: ("name", OneHotEncoder(dtype=np.uint8), [1, 2])
    
    sep : string
        Separator character.
    
    dtype : Numpy datatype
        The datatype of the samples.
    
    return_transformers : bool
        If True, return the label encoder and the normalization scaler.
    
    Returns
    -------
    X : ndarray of shape (n_samples, n_features)
        The samples.
    
    y : ndarray of shape (n_samples,)
        The target values (encoded from 0 to n_classes-1).
    
    label_encoder : sklearn.preprocessing.LabelEncoder (if return_transformers parameter is True)
        The transformer who changed the target values into 0, 1, ..., n_classes-1.
    
    scaler : sklearn.preprocessing.StandardScaler (if return_transformers parameter is True)
        The scaler used to normalize the samples.
    """

    X, y = [], []

    with open(filepath, 'r') as file:
        for line in file.readlines():
            sample = line.rstrip('\n').split(sep)

            y.append(sample.pop(target_position))
            
            if drop:
                for i in drop:
                    sample[i] = None
                _remove_from_iterable(sample)
                        
            X.append(sample)
    
    # Turn the list into a matrix
    X = np.array(X)
    
    # Encode the label (i. e. transform values into 0, 1, 2, ..., n_classes-1)
    label_encoder = preprocessing.LabelEncoder()
    y = label_encoder.fit_transform(y)

    # Encode features
    if features_to_transform:
        feature_encoder = compose.ColumnTransformer(transformers=features_to_transform, remainder="passthrough")
        X = feature_encoder.fit_transform(X)

    # Normalize the samples
    scaler = preprocessing.StandardScaler()
    
    # Results
    X = scaler.fit_transform(X).astype(dtype)
    y = np.array(y, dtype=int)

    if return_transformers:
        return X, y, label_encoder, scaler
    else:
        return X, y


# -----------------------------------------------------------------------------
#   learn
# -----------------------------------------------------------------------------

def learn(X, y, unlabeled, labeled, test, scores, estimator, strategy, n_queries, n_samples_per_iteration, \
          average=None, multi_class=None, method=0, **kwargs):
    """
    Learn actively the unlabeled pool of data and save the scores.

    Parameters
    ----------
    X : ndarray of shape (n_samples, n_features)
        The samples.
    
    y : ndarray of shape (n_samples,)
        The target values.
    
    unlabeled : list
        The indices of the unlabeled pool.
    
    labeled : list
        The indices of the learnt pool (essentially the bootstrap).
    
    test : list
        The indices of the test set.
    
    scores : list
        The different scores.
    
    estimator : callable
        The classifier to use.
    
    strategy : callable
        The query strategy to use.

    n_queries : int
        The number of queries.

    n_samples_per_iteration : int
        The number of samples to label per iteration.
    
    average : string
        Must be either "macro", "micro", "weighted", ...
    
    multi_class : string
        If specified, must be either "ovr" (One vs Rest) or "ovo" (One vs One).
    
    method : int or string
        See `criterion` argument from `ibelief.entropy()`.

    kwargs : dict
        Arguments for the estimator.
    """

    # -----------------------------------------------------------------
    # This variation from other `lib.py` files exists only for the
    # EKNNSampling method. It mustn't be forgotten for efficienty
    # purposes.
    if strategy == "eknn":
        eknnsample = EKNNSampling(X, y, labeled, unlabeled, method=method)
        strat_ = eknnsample.query
    elif strategy:
        strat_ = strategy
    else:
        strat_ = _random_sampling
    # -----------------------------------------------------------------
    
    learner = ActiveLearner(
        estimator      = estimator(**kwargs),
        query_strategy = strat_,

        X_training     = X[labeled],
        y_training     = y[labeled]
    )
    
    for _ in range(0, n_queries, n_samples_per_iteration):

        # `query_samples` is equal to `X[unlabeled][query_idx]`
        query_idx, query_samples = learner.query(X[unlabeled], n_instances=n_samples_per_iteration)
        learner.teach(query_samples, y[unlabeled][query_idx])

        # Update unlabeled and labeled sets
        _transfer(unlabeled, labeled, query_idx)

        # Scores
        # a = time.time()
        y_pred = learner.predict(X[test])
        # aa = round(time.time() - a, 2)
        # b = time.time()
        y_scores = learner.predict_proba(X[test])
        # bb = round(time.time() - b, 2)

        # print(f"predict: {aa} s")
        # print(f"proba:   {bb} s\n")
        # Specific case: in binary classification, the less represented will be considered as the positive
        if y_scores.shape[1] == 2:
            y_scores = y_scores[:, _min_dict(Counter(y[test]))]
        
        scores = _scoring(scores, y[test], y_pred, y_scores, average, multi_class)
    
    # _print_scores(scores)


# -----------------------------------------------------------------------------
#   print_results
# -----------------------------------------------------------------------------

def print_results(*args):
    """
    Print the final results for each dataset and each metrics.

    Parameters
    ----------
    args : tuple
        List of groups of results. Each group is a tuple, the first element must be a descriptive name and the second
        is the list of the results got from compare_scores().
    
    Example
    -------
    Let us say we got the following results:
    
    >>> A = compare_scores(...)
    >>> B = compare_scores(...)
    >>> C = compare_scores(...)
    >>> iris = compare_scores(...)
    >>> wine = compare_scores(...)
    
    There are two groups of results: (A, B, C) and (iris, wine). To print the final results:

    >>> print_results(
    ...     ("SYNTHETIC-LR", [A, B, C]),
    ...     ("REAL-LR",      [iris, wine])
    ... )
                    AUC     ACCU    F1      PREC    REC
    SYNTHETIC-LR    3/0/0   1/1/1   2/1/0   2/0/1   0/1/2
    REAL-LR         1/0/1   2/0/0   1/1/0   0/1/1   2/0/0
    """
    
    print(f"{'Dataset-method':15}\tAUC\tACC\tF1\tPREC\tREC")

    for group in args:
        print(f"{group[0]:15}", end='\t')
        
        for i in range(len(group[1][0])):
            win_tie_loss = [0, 0, 0]

            for results in group[1]:
                if results[i] == 1:
                    win_tie_loss[0] += 1
                elif results[i] == 0:
                    win_tie_loss[1] += 1
                elif results[i] == -1:
                    win_tie_loss[2] += 1
                else:
                    raise ValueError("Comparison's result must be 1, 0 or -1")
            
            print('/'.join([str(v) for v in win_tie_loss]), end='\t')
        
        print()


# -----------------------------------------------------------------------------
#   restore_graph
# -----------------------------------------------------------------------------

def restore_graph(title, filename_prefix,
                  classifiers, sampling_methods,
                  n_queries, n_samples_per_iteration,
                  ylimit=None):
    """
    Redraw a graph from .npy files.

    Parameters
    ----------
    title : string
        The title of th graph.
    
    filename_prefix : string
        The prefix of the filename to retrieve.
    
    classifiers : list of string
        The alias of the classifiers.
    
    sampling_methods : list of string
        The alias of the sampling methods.
    
    n_queries : int
    n_samples_per_iteration : int
        Must be the same as the integer used when the matrix has been stored.
    
    ylimit : list of two float
        Scale the y axis.

    Example
    -------

    >>> restore_graph("Skin Segmentation", {
        "RF": "g",
        "LR": "k",
        "5NN": "b",
        "E5NN": "m",
    }, {
        "UNC": "-",
        "RD": "-.",
    }, n_queries=20, n_samples_per_iteration=1)
    """

    def round_(x):
        return round(x, 3)

    curves = {}
    matrices = {}

    # Restore graph
    # for filename in files:
    #     with open(f"matrix/{filename}.npy", "rb") as f:
    #         matrices[filename] = []

    #         for legend in files[filename]:
    #             m = np.load(f)
    #             matrices[filename].append(m)
    #             curves[legend[0]] = (retrieve_scores(m, 1, n_queries//n_samples_per_iteration)[0], legend[1])
    
    for classifier in classifiers:
        filename = f"{filename_prefix}_{classifier}"

        with open(f"matrix/{filename}.npy", "rb") as f:
            matrices[filename] = []

            for sampling in sampling_methods:
                alias = f"{classifier}-{sampling}"

                m = np.load(f)
                matrices[filename].append(m)
                curves[alias] = (
                    retrieve_scores(m, 1, n_queries//n_samples_per_iteration)[0],
                    classifiers[classifier] + sampling_methods[sampling]
                )
    
    # Print matrices
    for data in matrices:
        matrix = matrices[data]
        print("---", data)
        
        for l in matrix:
            ll = l.tolist()
            for k in ll:
                print(list(map(round_, k)))
            print('.')
    
    visualise(title, n_queries, n_samples_per_iteration, ylimit, **curves)


# -----------------------------------------------------------------------------
#   retrieve_scores
# -----------------------------------------------------------------------------

def retrieve_scores(scores, n_metrics, measure_per_metrics):
    """
    Retrieve scores by metrics.
    
    Once every experiment is done, each measure is put in a matrix to make the computation of the mean more convenient.
    This function aims at splitting the different types of scores within the average matrix.

    Parameters
    ----------
    scores : ndarray of shape (n_experiments, n_metrics*n_measures)
        Matrix of scores.
    
    n_metrics : int
        Number of different metrics.
    
    measure_per_metrics : int
        Number of measures per metric.
    
    Returns
    -------
    split_scores : list of list
        The different averaged scores by type of metric.
    """

    scores = scores.mean(axis=0)
    return [ scores[measure_per_metrics*i:measure_per_metrics*(i+1)].tolist() for i in range(n_metrics) ]


# -----------------------------------------------------------------------------
#   test
# -----------------------------------------------------------------------------

def test(datatype, alias, X, y, classifiers, n_queries, steps, sampling_methods, test_size=.0909, method=0):
    """
    Launch a test on the specified dataset.

    Parameters
    ----------
    datatype : string
        Must be "syn" or "real".
    
    alias : string
        A name for the test.
    
    X : ndarray of shape (n_samples, n_features)
        The samples.
    
    y : ndarray of shape (n_samples,)
        The target values.
    
    classifiers : list of tuple
        Contains an iterable of (alias, classifier, matplotlib_plot_colour, arguments_for_classifier).
        For example: 
        >>> (
        ...     ("LR", LogisticRegression, "b", { "max_iter": 1000 }),
        ...     ("NB", GaussianNB,         "g", {}),
        ... )
        The alias will be used in the legend and in the filename, so it must contains compatible characters with file
        system.
    
    n_queries : int
        Number of queries.
    
    steps : int
        Number of samples to query per iteration.
    
    sampling_methods : tuple of two tuples (alias, callable)
        Contains an interable of (alias, sampling_callback, matplotlib_plot_style)
        For example:
        >>> (
        ...     ("UNC", uncertainty_sampling, "-"),
        ...     ("RD",  None,                 "-."),
        ... )
        If `sampling_callback` is `None`, the library will use the Random Sampling method. The alias will be used in
        the legend and in the filename, so it must contains compatible characters with the file system.
    
    test_size : float
        The proportion of the test set.
    
    method : int or string
        See `criterion` argument from `ibelief.entropy()`.
    
    Returns
    -------
    ret : dictionary
        The results in the following form:
        >>> ret
        {
            "RF-UNC": ([.50, .55, .58, .61, .70], "g-"),
            "RF-UNC": ([.45, .45, .51, .56, .56], "g-."),
        }
    """
    print(Counter(y))
    ret = {}
    start = time.time()
    
    for classifier in classifiers:
        subalias = f"{alias}_{classifier[0]}"
        substart = time.time()

        if datatype == "syn":
            sampling1, sampling2 = experiment_syn(subalias, X, y, classifier[1],
                                                  (sampling_methods[0][1], sampling_methods[1][1]), 
                                                  n_queries=n_queries, n_samples_per_iteration=steps, 
                                                  test_size=test_size,
                                                  method=method,
                                                  **classifier[3])
        elif datatype == "real":
            sampling1, sampling2 = experiment_real(subalias, X, y, classifier[1],
                                                   (sampling_methods[0][1], sampling_methods[1][1]),
                                                   n_queries=n_queries, n_samples_per_iteration=steps,
                                                   test_size=test_size, 
                                                   method=method,
                                                   **classifier[3])
        else:
            raise ValueError("datatype must be real or syn")
        
        print(f"  [ {subalias} done in {round(time.time() - substart, 2)} s ] ({classifier[0]})")
        ret[f"{classifier[0]}-{sampling_methods[0][0]}"] = (sampling1, f"{classifier[2]}{sampling_methods[0][2]}")
        ret[f"{classifier[0]}-{sampling_methods[1][0]}"] = (sampling2, f"{classifier[2]}{sampling_methods[1][2]}")

        print(f"       {classifier[0]}-{sampling_methods[0][0]:6}: {stats.t.interval(alpha=.95, df=len(sampling1)-1, loc=np.mean(sampling1), scale=stats.sem(sampling1))}")
        print(f"       {classifier[0]}-{sampling_methods[1][0]:6}: {stats.t.interval(alpha=.95, df=len(sampling2)-1, loc=np.mean(sampling2), scale=stats.sem(sampling2))}")
    
    print(f"{alias} achieved in {round(time.time() - start, 2)} s")
    return ret


# -----------------------------------------------------------------------------
#   train_test_indices
# -----------------------------------------------------------------------------

def train_test_indices(X, y, test_size=.25, seed=None):
    """
    Returns the indices of the samples for the train and test sets.

    Parameters
    ----------
    X : ndarray of shape (n_samples, n_features)
        The samples.
    
    y : ndarray of shape (n_samples,)
        The target values.
    
    test_size : float
        The proportion of data to keep in the test set (must be in [0, 1]).
    
    seed : int
        Initialize the random generator. For reproducibility.
    
    Returns
    -------
    train : list
        The indices of the samples for the training set.
    
    test : list
        The indices of the samples for the test set.
    """

    sss = model_selection.StratifiedShuffleSplit(n_splits=1, test_size=test_size, random_state=seed)
    train, test = next(sss.split(X, y))

    return train.tolist(), test.tolist()


# -----------------------------------------------------------------------------
#   visualise
# -----------------------------------------------------------------------------

def visualise(title, n_queries, n_samples_per_iteration, ylimit=None, **kwargs):
    """
    Plot the learning curves.

    Parameters
    ----------
    title : string
        Title of the graph.
    
    n_queries : int
        Maximum of x absciss.
    
    n_samples_per_iteration : int
        Step between each x.
    
    ylimit : list or None
        Must be a list of two floats if specified.
        If specified, the y axis will be set according to the argument.
    
    **kwargs : dict
        Graph to plot. Must be of the following form:

        >>> kwargs["legend_of_graph"] = (
        ...     list_of_graph_values,
        ...     colour
        ... )
    """
    
    fig, ax = plt.subplots(figsize=(8, 5), dpi=100)
    x = list(range(0, n_queries, n_samples_per_iteration))

    for label in kwargs:
        ax.plot(x, kwargs[label][0], kwargs[label][1], label=label)

    ax.set_title(title)
    ax.set_ylabel("Exactitude")
    ax.set_xlabel("Nombre de requêtes")

    if ylimit:
        ax.set_ylim(ylimit[0], ylimit[1])
    ax.legend(loc="lower right")

    plt.show()


# -----------------------------------------------------------------------------
#   Internal function: _min_dict
# -----------------------------------------------------------------------------

def _min_dict(dictionary):
    """
    Returns the key of the minimum value in a dictionary.

    Parameters
    ----------
    dictionary : dict
        A dictionary with numeric values.
    
    Returns
    -------
    min_index : object
        The key of the minimum value of the dictionary.
    """

    min_index = None

    for index in dictionary:
        if not min_index or dictionary[min_index] > dictionary[index]:
            min_index = index
    
    return min_index


# -----------------------------------------------------------------------------
#   Internal function: _print_scores
# -----------------------------------------------------------------------------

def _print_scores(scores):
    """
    Print the scores.

    Parameters
    ----------
    scores : list
        The scores to print.
    """

    def round_(value, dec=3):
        return round(value, dec)
    
    # print(f"AUC ROC:   {list(map(round_, scores[0]))}")
    print(f"ACCURACY:  {list(map(round_, scores[1]))}")
    # print(f"F1 SCORE:  {list(map(round_, scores[2]))}")
    # print(f"PRECISION: {list(map(round_, scores[3]))}")
    # print(f"RECALL:    {list(map(round_, scores[4]))}")


# -----------------------------------------------------------------------------
#   Internal function: _random_sampling
# -----------------------------------------------------------------------------

def _random_sampling(estimator, X, n_instances=1, rng=np.random.default_rng()):
    """
    Sample randomly data from the pool.
    
    Parameters
    ----------
    estimator : callable
        The classifier to use.
    
    X : ndarray of shape (n_samples, n_features)
        The samples.
    
    n_instances : int
        The number of samples to sample.
    
    rng : Numpy Random Generator
        The random generator (can be initialized with a seed).
    """

    samples = list(range(X.shape[0]))
    query_idx = np.empty(n_instances, dtype=np.uint8)
    
    for i in range(len(query_idx)):
        query_idx[i] = samples.pop(rng.integers(len(samples)))
    
    return query_idx, X[query_idx]


# -----------------------------------------------------------------------------
#   Internal function: _remove_from_iterable
# -----------------------------------------------------------------------------

def _remove_from_iterable(iterable, value=None):
    """
    Remove all occurence of a specified value from an iterable.

    Parameters
    ----------
    iterable : list
        The list to work on.
    
    value : object
        The value to remove from the list. Can be an iterable of elements to remove.
    
    Returns
    -------
    iterable : list
        The modified list.
    """
    
    try:
        for i in range(len(iterable)-1, -1, -1):
            if iterable[i] in value:
                iterable.pop(i)
    
    except TypeError:
        for i in range(len(iterable)-1, -1, -1):
            if iterable[i] == value:
                iterable.pop(i)
    
    return iterable


# -----------------------------------------------------------------------------
#   Internal function: _scoring
# -----------------------------------------------------------------------------

def _scoring(scores, y_true, y_pred, y_score, average="macro", multi_class=None, zero_division=1):
    """
    Compute the scores.

    Parameters
    ----------
    scores : list
        Contains the different scores.
    
    y_true : ndarray of shape (n_samples,)
        The actual target values.
    
    y_pred : ndarray of shape (n_samples,)
        The predicted target values.
    
    y_score : ndarray of shape (n_samples, n_features)
        The probability of prediction of target values.
    
    average : string
        Must be either "macro", "micro", "weighted", ...
    
    multi_class : string
        If specified, must be either "ovr" (One vs Rest) or "ovo" (One vs One).
    
    zero_division : 0, 1 or "warn"
        Default behaviour if a division by zero happens (occurs when some classes are never predicted).

    Returns
    -------
    scores : list
        The list modified.
    """

    # scores[0].append(
    #     metrics.roc_auc_score(y_true, y_score, average=average, multi_class=multi_class)
    # )

    scores.append(
        metrics.accuracy_score(y_true, y_pred)
    )

    # scores[2].append(
    #     metrics.f1_score(y_true, y_pred, average=average, zero_division=zero_division)
    # )

    # scores[3].append(
    #     metrics.precision_score(y_true, y_pred, average=average, zero_division=zero_division)
    # )

    # scores[4].append(
    #     metrics.recall_score(y_true, y_pred, average=average, zero_division=zero_division)
    # )

    return scores


# -----------------------------------------------------------------------------
#   Internal function: _transfer
# -----------------------------------------------------------------------------

def _transfer(a, b, indices):
    """
    Transfer the values of the sub-matrix a[indices] to b.

    Parameters
    ----------
    a : list
        The source data.
    
    b : list
        The target matrix.
    
    indices : list
        List of indices of a to move to b.
    """

    for index in indices:
        b.append(a[index])
        a[index] = None
    
    _remove_from_iterable(a)

