#
#
#
#
#   iBelief library, adapted from https://github.com/jusdesoja/iBelief_python
#
#
#
#

import math
import numpy as np


# -----------------------------------------------------------------------------
#   iBelief
# -----------------------------------------------------------------------------

def entropy(mass, criterion):
    """
    Calculate entropy of the mass function.

    Parameters
    ----------
    mass: ndarray
        Mass function to calculate entropy
    
    criterion: integer
        Criterion index for different entropy
            1: Höhle's entropy
            2: Smets's entropy
            3: Yager entropy
            4: Nguyen entropy
            5: Dubois and Prad entropy
            6: Pal entropy
            7: Maeda and Ichihashi entropy
            8: Harmanec and Klir entropy
    
    Returns
    -------
    h : float
        The calculated entropy.
    """

    # Höhle
    if criterion == 1 or criterion == "hohle":
        bel = mtobel(mass)
        h = np.sum(mass[1:] * np.log2(1/bel[1:]))
    
    # Smets
    elif criterion == 2 or criterion == "smets":
        if mass[-1] != 0:
            q = mtoq(mass)
            h = np.sum(np.log2(1/q[1:]))
        else:
            h = float("inf")
    
    # Yager
    elif criterion == 3 or criterion == "yager":
        pl = mtopl(mass)
        h = np.sum(mass[1:] * np.log2(1/pl[1:]))

    # Nguyen
    elif criterion == 4 or criterion == "nguyen":
        h = np.sum(mass[1:] * np.log2(1/mass[1:]))

    # Dubois and Prade
    elif criterion == 5 or criterion == "dubois" or criterion == "prade":
        absInd = np.array([bin(i).count('1') for i in range(mass.size)])
        h = np.sum(mass[1:] * np.log2(absInd[1:]))
    
    # Pal
    elif criterion == 6 or criterion == "pal":
        absInd = np.array([bin(i).count('1') for i in range(mass.size)])
        h = np.sum(mass[1:] * np.log2(absInd[1:] / mass[1:]))
    
    # Maeda and Ichihashi
    elif criterion == 7 or criterion == "maeda" or criterion == "ichihashi":
        raise NotImplementedError()

    # Harmanec and Klir
    elif criterion == 8 or criterion == "harmanec" or criterion == "klir":
        raise NotImplementedError()
    
    # Error
    else:
        raise ValueError("The specified criterion is unknown")

    return h


def mtobel(InputVec):
    return mtob(mtonm(InputVec))


def mtob(InputVec):
    """
    Comput InputVec from m to b function.  belief function + m(emptset)

    Parameter
    ---------
    InputVec: vector m representing a mass function

    Return
    ---------
    out: a vector representing a belief function
    """
    InputVec = InputVec.copy()
    mf = InputVec.size
    natoms = round(math.log(mf, 2))
    if math.pow(2, natoms) == mf:
        for i in range(natoms):
            i124 = int(math.pow(2, i))
            i842 = int(math.pow(2, natoms - i))
            i421 = int(math.pow(2, natoms - i - 1))
            InputVec = InputVec.reshape(i124, i842, order='F')
            # for j in range(1, i421 + 1): #to be replaced by i842
            for j in range(i421):  # not a good way for add operation coz loop matrix for i842 times
                InputVec[:, j * 2 + 1] = InputVec[:,
                    j * 2 + 1] + InputVec[:, j * 2]
        out = InputVec.reshape(1, mf, order='F')[0]
        return out
    else:
        raise ValueError(
            "ACCIDENT in mtoq: length of input vector not OK: should be a power of 2, given %d\n" % mf)


def mtonm(InputVec):
    """
    Transform bbm into normalized bbm

    Parameter
    ---------
    InputVec: vector m representing a mass function

    Return
    ---------
    out: vector representing a normalized mass function
    """
    if InputVec[0] < 1:
        out = InputVec/(1-InputVec[0])
        out[0] = 0
    return out


def mtobetp(InputVec):
    """Computing pignistic propability BetP on the signal points from the m vector (InputVec) out = BetP
    vector beware: not optimize, so can be slow for more than 10 atoms
    Parameter
    ---------
    InputVec: a vector representing a mass function
    Return
    ---------
    out: a vector representing the correspondant pignistic propability
    """
    # the length of the power set, f
    mf = InputVec.size
    # the number of the signal point clusters
    natoms = round(math.log(mf, 2))
    if math.pow(2, natoms) == mf:
        if InputVec[0] == 1:
            # bba of the empty set is 1
            exit("warning: all bba is given to the empty set, check the frame\n")
            out = np.ones(natoms)/natoms
        else:
            betp = np.zeros(natoms)
            for i in range(1, mf):
                # x , the focal sets InputVec the dec2bin form
                x = np.array(list(map(int, np.binary_repr(i, natoms)))[
                             ::-1])  # reverse the binary expression
                # m_i is assigned to all the signal points equally

                betp = betp + np.multiply(InputVec[i]/sum(x), x)
            out = np.divide(betp, (1.0 - InputVec[0]))
        return out
    else:
        raise ValueError(
            "Error: the length of the InputVec vector should be power set of 2, given %d \n" % mf)


def mtopl(InputVec):
    """
    Compute from mass function m to plausibility pl.

    Parameter
    ----------
    InputVec: mass function m

    Return
    --------
    output: plausibility function pl
    """
    InputVec = mtob(InputVec)
    out = btopl(InputVec)
    return out


def btopl(InputVec):
    """
    Compute from belief b to plausibility pl

    Parameter
    ---------
    InputVec: belief function b

    Return
    ------
    out: plausibility function pl
    """

    lm = InputVec.size
    natoms = round(math.log2(lm))
    if 2 ** natoms == lm:
        InputVec = InputVec[-1] - InputVec[::-1]
        out = InputVec
        return out
    else:
        raise ValueError("ACCIDENT in btopl: length of input vector not OK: should be a power of 2, given %d\n" % lm)


def mtoq(InputVec):
    """
    Computing Fast Mobius Transfer (FMT) from mass function m to commonality function q

    Parameters
    ----------
    InputVec : vector m representing a mass function

    Return:
    out: a vector representing a commonality function
    """
    InputVec = InputVec.copy()
    mf = InputVec.size
    natoms = round(math.log2(mf))
    if 2 ** natoms == mf:
        for i in range(natoms):
            i124 = int(math.pow(2, i))
            i842 = int(math.pow(2, natoms - i))
            i421 = int(math.pow(2, natoms - i - 1))
            InputVec = InputVec.reshape(i124, i842, order='F')
            # for j in range(1, i421 + 1): #to be replaced by i842
            for j in range(i421):  # not a good way for add operation coz loop matrix for i842 times
                InputVec[:, j * 2] = InputVec[:, j * 2] + InputVec[:, j * 2+1]
        out = InputVec.reshape(1, mf, order='F')[0]
        return out
    else:
        raise ValueError(
            "ACCIDENT in mtoq: length of input vector not OK: should be a power of 2, given %d\n" % mf)
