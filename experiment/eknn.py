"""
A Python implementation of the evidential k nearest neighbours
described by T. Denoeux.

Translated from the R package EKNN.

Requirements
------------
numpy (>= 1.20.1)
scikit-learn (>= 0.24.1)
"""

import hashlib

import multiprocessing as mp
import numpy as np

from collections import Counter, deque

from sklearn import preprocessing
from sklearn import model_selection
from sklearn.base import BaseEstimator, ClassifierMixin
from sklearn.exceptions import NotFittedError
from sklearn.metrics import accuracy_score


# -----------------------------------------------------------------------------
# Parameters
# -----------------------------------------------------------------------------

# Minimum amount of samples if the use of multiprocessing is requested
MIN_N_SAMPLES = 100

# Maximum amount of training samples per iteration
MAX_N_TRAINING_SAMPLES = 100


# -----------------------------------------------------------------------------
# EKNN
# -----------------------------------------------------------------------------

class EKNN(BaseEstimator, ClassifierMixin):
    """
    Evidental k nearest neighbours (EKNN) classifier by Denoeux (1995).

   	The decision rule corresponds together to the minimum of the upper,
    lower and pignistic risks in the case of {0,1} costs: the chosen
    class is the one for which the value of the Basic Probability
    Assignment (BPA) is maximum.
    """

    def __init__(self, n_neighbors=5, multiprocesses=True):
        """
        Parameters
        ----------
        n_neighbors : int
            Number of neighbors.
        
        multiprocesses : bool
            If True, the classifier will use several CPU to compute.
            No multiprocessing will be used if less than
            eknn.MIN_N_SAMPLES samples are provided.
        
        Returns
        -------
        EKNN
            The current instance.
        """
        self.n_neighbors = n_neighbors
        self.multiprocesses = multiprocesses

        self._fitted = False
    

    def fit(self, X, y):
        """
        Train the classifier.

        Parameters
        ----------
        X : ndarray of shape (n_samples, n_features)
            The samples to learn.
        
        y : ndarray of shape (n_samples,)
            The target values of the samples to learn.
        
        Returns
        -------
        self : EKNN
            The current instance.
        """
        if X.shape[0] != y.shape[0]:
            raise ValueError("X and y must have the same number of rows")
        
        if len(y.shape) > 1:
            y = y.reshape(-1,)
        
        # Label encoders
        self.label_encoder = preprocessing.LabelEncoder()
        
        # Save data
        self.X_trained = X
        self.y_trained = self.label_encoder.fit_transform(y)
        
        self.classes_ = self.label_encoder.classes_

        # Init parameters
        self._init_parameters()
        self._fitted = True
        self._saved  = [None, None] # [0] hash [1] saved bpa

        return self


    def get_params(self, deep=True):
        """
        Return the parameters of the classifier.

        Parameters
        ----------
        deep : bool
            Ignored.

        Returns
        -------
        dict
            Containing the parameters and their values.
        """
        return {
            "n_neighbors": self.n_neighbors,
            "multiprocesses": self.multiprocesses
        }


    def predict(self, X, with_ignorance=False, _return=None, _distances=None):
        """
        Predict the target values of the given samples.
        
        Parameters
        ----------
        X : ndarray of shape (n_samples, n_features)
            The samples to predict.
        
        with_ignorance : bool
            If True, the ignorance decision is returned when the mass
            assigned to it is higher than any other class.
        
        _return : str
            Internal parameter. If "bpa" (respectively "dist"), return
            the BPA (respectively distances) and not the predictions.
        
        _distances : tuple of two ndarray of shape (n_samples, n_neighbors)
        and (n_samples, n_neighbors)
            Take theses precomputed distances.
        
        Returns
        -------
        y : ndarray of shape (n_samples,)
            The target values predicted for the given samples.
        
        OR (if _return == "bpa")

        ndarray of shape (n_samples, n_classes+C) with C in {0; 1}
            Array of the final bpa. Each row is a bpa of a specific
            sample of X and each column is the mass of a specific class.
        
        OR (if _return == "dist")
        
        ndarray of shape (n_samples, n_neighbors)
            Distances of the n_neighbors nearest neighbors of each
            sample.
        
        ndarray of shape (n_samples, n_neighbors)
            Indices of the n_neighbors nearest neighbors of each
            sample.
        """
        if not self._fitted:
            raise NotFittedError("The classifier hasn't been fitted yet")
        if _return not in (None, "bpa", "dist"):
            raise ValueError("_return must either be None, bpa or dist")
        
        # The given samples correspond to the last prediction so do not
        # recompute it again
        if _hash(X) == self._saved[0]:
            if _return:
                self._saved[1]
            else:
                y = np.argmax(self._saved[1], axis=1)
                return self.label_encoder.inverse_transform(y)
        
        # Without multiprocessing
        if not self.multiprocesses or self.y_trained.shape[0] < MIN_N_SAMPLES:
            _, res = _predict(
                    None,
                    np.arange(X.shape[0]),
                    X,
                    self.X_trained,
                    self.y_trained,
                    self.n_neighbors,
                    with_ignorance,
                    self.label_encoder.transform(self.classes_),
                    self.alpha,
                    self.gamma,
                    _return,
                    _distances,
                )

        # With multiprocessing
        else:
            n_processes = mp.cpu_count()-1 if mp.cpu_count() > 1 else 1

            if _return == "bpa" or _return is None:
                res = np.zeros((X.shape[0], self.classes_.shape[0]))
            elif _return == "dist":
                res = (
                    np.zeros((X.shape[0], self.n_neighbors)),
                    np.zeros((X.shape[0], self.n_neighbors), dtype=int)
                )
            
            indices = np.array_split(np.arange(X.shape[0]), n_processes)

            # Create processes and queue, then start the processes
            processes = []
            queue     = mp.Queue()

            for i in range(n_processes):
                distances_i = None if _distances is None \
                                   else (_distances[0][indices[i]],
                                         _distances[1][indices[i]])
                
                processes.append(mp.Process(
                    target=_predict,
                    args=(
                        queue, indices[i], X[indices[i]], self.X_trained,
                        self.y_trained, self.n_neighbors, with_ignorance,
                        self.label_encoder.transform(self.classes_),
                        self.alpha, self.gamma, _return, distances_i,
                    )
                ))
            
            for process in processes:
                process.start()
            
            # Collect the results computed by child processes
            for _ in range(n_processes):
                process_result = queue.get()

                if _return != "dist":
                    res[process_result[0]] = process_result[1]
                else:
                    res[0][process_result[0]] = process_result[1][0]
                    res[1][process_result[0]] = process_result[1][1]
            
            # End of multiprocessing
            for process in processes:
                process.join()

        if _return != "dist":
            self._saved[0] = _hash(X)
            self._saved[1] = res

        # Results
        if _return:
            return res
        else:
            y = np.argmax(res, axis=1)
            return self.label_encoder.inverse_transform(y)


    def predict_proba(self, X, with_ignorance=False):
        """
        Return the classification probabilities.
        
        Parameters
        ----------
        X : ndarray of shape (n_samples, n_features)
            The samples to predict.
        
        with_ignorance : bool
            If True, the ignorance decision is returned when the mass
            assigned to it is higher than any other class.
        
        Returns
        -------
        ndarray of shape (n_samples,)
            The classification probabilities of the given samples.
        """
        bpa   = self.predict(X, with_ignorance, _return="bpa")
        proba = np.zeros(bpa.shape)
        
        normalizer = bpa.sum(axis=1)
        
        for i in range(proba.shape[0]):
            proba[i] = bpa[i] / normalizer[i]
        
        return proba


    def predict_log_proba(self, X, with_ignorance=False):
        """
        Return the logarithm of the classification porbabilities.
        
        Parameters
        ----------
        X : ndarray of shape (n_samples, n_features)
            The samples to predict.
        
        with_ignorance : bool
            If True, the ignorance decision is returned when the mass
            assigned to it is higher than any other class.
        
        Returns
        -------
        ndarray of shape (n_samples,)
            The logarithm of the classification probabilities.
        """
        return np.log(self.predict_proba(X, with_ignorance))


    def score(self, X, y_true):
        """
        Return the accuracy score of the given samples.

        Parameters
        ----------
        X : ndarray of shape (n_samples, n_features)
            The samples to evaluate.
        
        y_true : ndarray of shape (n_samples,)
            The true target values of the given samples.
        
        Returns
        -------
        score : float
            The accuracy score.
        """
        y_pred = self.predict(X)
        return accuracy_score(y_true, y_pred)


    def set_params(self, n_neighbors, multiprocesses, deep=True):
        """
        Modify the parameters of the classifier.

        Parameters
        ----------
        n_neighbors : int
            Number of neighbors.
        
        multiprocesses : bool
            If True, the classifier will use several CPU to compute.
            No multiprocessing will be used if less than
            eknn.MIN_N_SAMPLES samples are provided.
        
        deep : bool
            Ignored.
        """
        self.n_neighbors = n_neighbors
        self.multiprocesses = multiprocesses


    def _init_parameters(self, alpha=.95):
        """
        Set the alpha and gamma parameters.
        See A k-Nearest Neighbor Classification Rule Based on Dempster-
        Shafer Theory (Denoeux, 1995) for further details.

        Parameters
        ----------
        alpha : float in [0., 1.]
            Default value for alpha.
        """
        mean_distances = np.zeros(self.classes_.shape[0])
        concatenated = np.column_stack((self.X_trained, self.y_trained))

        # Without multiprocessing
        if  not self.multiprocesses or self.y_trained.shape[0] < MIN_N_SAMPLES:
            result = _compute_gamma(concatenated)
            
            for key in result:
                mean_distances[key] = result[key][0]
            
            self.alpha = alpha
            self.gamma = 1 / mean_distances
        
        # With multiprocessing
        else:
            n_processes = mp.cpu_count()-1 if mp.cpu_count() > 1 else 1
            samples = _eknn_split(concatenated, n_processes)
            
            weights = { key: 0 for key in range(self.classes_.shape[0]) }
            
            with mp.Pool(processes=n_processes) as p:
                result = p.imap(_compute_gamma, samples)

                for res in result:
                    for key in res:
                        mean_distances[key] = (
                              weights[key] * mean_distances[key]
                            + res[key][1]  * res[key][0]
                        ) / (weights[key] + res[key][1])
                        weights[key] += res[key][1]
            
            self.alpha = alpha
            self.gamma = 1 / mean_distances


# -----------------------------------------------------------------------------
# Other internal functions
# -----------------------------------------------------------------------------

def _compute_bpa(
        X, y_train, distances, indices, alpha,
        gamma, n_neighbors, with_ignorance, classes
    ):
    """
    Compute the basic probability assignments.

    Parameters
    ----------
    X : ndarray of shape (n_samples, n_features)
        The samples to predict.
    
    y_train : ndarray of shape (n_samples,)
        The target values of the trained sample.
    
    distances : ndarray of shape (n_neighbors, n_samples)
        Distances of the n_neighbors nearest neighbors of each sample.

    indices : ndarray of shape (n_neighbors, n_samples)
        Indices of the n_neighbors nearest neighbors of each sample.
    
    alpha : float
        Parameter computed by the classifier in the fitting phase.
    
    gamma : ndarray of shape (n_classes,)
        Parameter computed by the classifier in the fitting phase.
    
    n_neighbors : int
        The number of nearest neighbors that matter.
    
    with_ignorance : bool
        If True, the ignorance decision is returned when the mass
        assigned to it is higher than any other class.
    
    classes : ndarray of shape (n_classes)
        The classes of the whole dataset.
    
    Returns
    -------
    bpa : ndarray of shape (n_samples, n_classes+C) with C in {0; 1}
        Array of the final bpa. Each row is a bpa of a specific sample
        of X and each column is the mass of a specific class.
    """
    n_samples = X.shape[0]

    n_classes = classes.shape[0]

    bpa = np.zeros((n_classes+1, n_samples))
    bpa[-1] = 1

    for i in range(n_samples):
        for j in range(n_neighbors):
            index_neighbors = indices[j, i]
            sample_label = y_train[index_neighbors]

            m1 = np.zeros(n_classes+1)
            m1[sample_label] = alpha * np.exp(
                -gamma[sample_label]**2 * distances[j, i]
            )
            m1[-1] = 1 - m1[sample_label]

            # Conjunctive rule of Dempster
            a = m1[:n_classes,].reshape(-1,) * bpa[:n_classes, i]
            b = m1[:n_classes,].reshape(-1,) * bpa[n_classes, i]
            c = bpa[:n_classes, i] * m1[n_classes]

            bpa[:n_classes, i] = a + b + c
            bpa[n_classes, i] = m1[n_classes] * bpa[n_classes, i]
    
    # Normalization by column (each column is a BPA)
    unit = [1 for _ in range(1, n_classes+2)]
    vectored_unit = np.array(unit).reshape(n_classes+1, 1)

    col_sum = np.sum(bpa, axis=0)
    bpa = bpa / np.dot(vectored_unit, col_sum.reshape(1, -1))

    return bpa.T if with_ignorance else bpa[0:n_classes].T


def _compute_distances(X, X_train, n_neighbors):
    """
    Compute and retrieve the nearest neighbors of samples.

    Parameters
    ----------
    X : ndarray of shape (n_samples, n_features)
        The samples for which the distances have to be computed.
    
    X_train : ndarray of shape (n_samples', n_features)
        The training set.
    
    n_neighbors : int
        The number of nearest neighbors that matter.
    
    Returns
    -------
    ndarray of shape (n_neighbors, n_samples)
        Distances of the n_neighbors nearest neighbors of each sample.
    
    ndarray of shape (n_neighbors, n_samples)
        Indices of the n_neighbors nearest neighbors of each sample.
    """
    if X.shape[1] != X_train.shape[1]:
        raise ValueError(
            "X and X_train must have the same number of columns"
        )
    
    indices = []
    distances = []

    for i in range(X.shape[0]):
        sample = X[i]
        vectored_sample = np.array([sample]*X_train.shape[0])

        # NOTE: No square root is used in the R version of this code
        # but the order is the same
        local_distances = np.sum(
            (vectored_sample - X_train)**2,
            axis=1
        )

        # Retrieve the k nearest neighbors
        sorted_indices = np.argsort(local_distances)[:n_neighbors]
        sorted_distances = local_distances[sorted_indices]

        distances.append(sorted_distances)
        indices.append(sorted_indices)
    
    return np.array(distances).T, np.array(indices).T


def _compute_gamma(samples):
    """
    Compute the gamma parameters.
    See A k-Nearest Neighbor Classification Rule Based on Dempster-
    Shafer Theory (Denoeux, 1995) for further details.

    Parameters
    ----------
    samples : ndarray of shape (n_samples, n_features+1)
        The concatenation of the samples and the target values.

    Returns
    -------
    float
        The mean distances between every samples from the same class.
    """
    X = samples[:, :-1]
    y = samples[:, -1]
    
    classes = np.unique(y).astype(int)
    mean_distances = dict()

    for label in range(classes.shape[0]):
        label_indices = np.where(y == classes[label])[0]
        n_index = len(label_indices)

        distances = np.zeros((n_index, n_index))
        
        for i, label_index in enumerate(label_indices):
            sample = X[label_index]
            distances[i] = np.sqrt(
                np.sum(
                    (np.array([sample]*n_index) - X[label_indices])**2,
                    axis=1
                )
            )
        
        divisor = (n_index**2 - n_index) if n_index > 1 else 1
        
        mean_distances[classes[label]] = (
            np.sum(distances / divisor),
            n_index,
        )

    return mean_distances


def _hash(array):
    """
    Hash an array.

    Parameters
    ----------
    array : ndarray
        The array to hash.
    
    Returns
    -------
    hash :
        The hash got from the array.
    """
    return hashlib.sha224(' '.join(list(map(str, array))).encode('utf-8')).hexdigest()


def _predict(
        queue, indices, X, X_train, y_train, n_neighbors, with_ignorance,
        classes, alpha, gamma, _return=None, _distances=None
    ):
    """
    Predict the target values of the given samples.
    
    Parameters
    ----------
    queue : mp.Queue
        The queue used to return the result to the parent process.
        If the argument is None, the target values will be returned.
    
    indices : ndarray of shape (n_samples,)
        Indices of the samples to predict.

    X : ndarray of shape (n_samples, n_features)
        The samples to predict.
    
    X_train : ndarray of shape (n_samples', n_features)
        Training set.
    
    y_train : ndarray of shape (n_samples',)
        Target values of the training set.
    
    n_neighbors : int
        The number of nearest neighbors that matter.
    
    with_ignorance : bool
        If True, the ignorance decision is returned when the mass
        assigned to it is higher than any other class.
    
    classes : ndarray of shape (n_classes)
        The classes of the whole dataset.
    
    alpha : float
        Parameter computed by the classifier in the fitting phase.
    
    gamma : ndarray of shape (n_classes,)
        Parameter computed by the classifier in the fitting phase.
    
    _return : str
        Internal parameter. If "bpa" (respectively "dist"), return
        the BPA (respectively distances) and not the predictions.
        
    _distances : tuple of two ndarray of shape (n_samples, n_neighbors)
    and (n_samples, n_neighbors)
        Take theses precomputed distances.
    
    Returns
    -------
    If the argument queue is not None:
    
    indices : ndarray of shape (n_neighbors,)
        Indices of the n_neighbors nearest neighbors.
    
    (
        bpa : ndarray of shape (n_samples, n_classes+C) with C in {0; 1}
            Array of the final bpa. Each row is a bpa of a specific
            sample of X and each column is the mass of a specific class.
        
        OR (if _return_distances is True)

        (
            ndarray of shape (n_samples, n_neighbors)
                Distances of the n_neighbors nearest neighbors of each
                sample.
            
            ndarray of shape (n_samples, n_neighbors)
                Indices of the n_neighbors nearest neighbors of each
                sample.
        )
    )
    """
    # Compute or retrieve distances
    if _distances:
        distances, distances_indices = _distances[0].T, _distances[1].T
    else:
        distances, distances_indices = _compute_distances(
            X, X_train, n_neighbors
        )
    
    # Return distances
    if _return == "dist":
        result = distances.T, distances_indices.T
    
    # Return BPA
    else:
        result = _compute_bpa(
            X, y_train, distances, distances_indices, alpha,
            gamma, n_neighbors, with_ignorance, classes
        )

    # Results
    if queue:
        queue.put((indices, result))
    else:
        return indices, result


def _eknn_split(samples, n_splits, label_column=-1):
    """
    Split the samples into n_splits of splits such that each
    class is represented in only one split.

    Parameters
    ----------
    samples : ndarray of shape (n_samples, n_features+1)
        The samples to split.
    
    n_splits : int
        Number of splits.
    
    label_column : int
        Index of the label column.
    
    Returns
    -------
    list of ndarray of (almost) equal shapes
        The sub samples.
    """
    if samples.shape[0]-1 < n_splits:
        raise ValueError("Too much split for the number of samples")
    
    counter = Counter(samples[:, label_column])
    least_represented = min(counter, key=counter.get)

    if counter[least_represented] < 2:
        raise ValueError(
            f"{least_represented} must be represented at least twice"
        )

    samples = samples[samples[:, label_column].argsort()] # Sort by label
    queue   = deque(samples)
    splits = [[] for _ in range(n_splits)]
    
    limit = np.ceil(samples.shape[0] / n_splits)

    # Fill the subsample one by one
    for i, split in enumerate(splits):
        n_already_added_samples = 0
        last_sample = None

        while queue:
            sample = queue.popleft()

            # Same class: add it
            if last_sample is not None \
               and sample[label_column] == last_sample[label_column]:
                split.append(sample)
            
            # Different class: might need to change split
            else:
                if n_already_added_samples >= limit and i < n_splits-1:
                    queue.appendleft(sample)
                    break
                else:
                    split.append(sample)

            last_sample = sample
            n_already_added_samples += 1
    
    return [res for res in map(np.array, splits) if res.any()]