"""
Sampling method made with the EKNN classifier.
"""

import numpy as np

from .ibelief import mtobetp, entropy
from .eknn import EKNN


# -----------------------------------------------------------------------------
#   EKNNSampling
# -----------------------------------------------------------------------------

class EKNNSampling:
    """
    Sampling method made with the EKNN classifier. It computes and stores
    the the shortest distances between the samples from the traning set
    and the pool.

    Parameters
    ----------
    X : ndarray of shape (n_samples, n_features)
        The samples.
    
    y : ndarray of shape (n_samples,)
        The target values.
    
    train : list
        Indices of the samples of the training set.
    
    pool : list
        Indices of the samples of the pool.
    
    n_neighbors : int
        Number of neighbors that matter.
    """

    def __init__(self, X, y, train, pool, n_neighbors=1, method=0):
        self.X = X
        self.y = y

        self.train = train
        self.pool  = pool

        self.method = method

        self.classifier = EKNN(n_neighbors=n_neighbors, multiprocesses=True)
        self.classifier.fit(X[train], y[train])
        
        self._compute_distances()
    

    def query(self, classifier, X, n_instances=1):
        """
        Choose the most relevant samples from the given pool.

        Parameters
        ----------
        classifier : instance of modAL.models.ActiveLearner
            The learner to use.
        
        X : ndarray of shape (n_samples, n_features)
            The pool's samples. Must absolutely be equal (element wise)
            to self.X[train].
        
        n_instances : int
            The number of samples to sample.
        
        Returns
        -------
        query_idx : ndarray of shape (n_instances,)
            The indices of the selected samples.
        
        query_samples : ndarray of shape (n_instances, n_features)
            The selected samples.
        """
        if X.shape[0] < n_instances:
            n_instances = X.shape[0]
        if (X != self.X[self.pool]).any():
            raise ValueError("X does not seem to be correct!")
        
        query_idx = []
        
        bpa_matrix = self._bpa_to_matrix(X)

        if self.method:
            am = entropy(bpa_matrix, self.method)
        else:
            am = self._ambiguity_measure(bpa_matrix)

        # Select the n_instances best samples
        for _ in range(n_instances):
            max_am_value = -1
            max_am_index = -1

            for i in range(X.shape[0]):
                am_value, am_index = am[i], i
                
                # TODO: To improve!
                if am_value > max_am_value and am_index not in query_idx:
                    max_am_value = am_value
                    max_am_index = am_index
            
            query_idx.append(max_am_index)
        
        return np.array(query_idx), X[query_idx]


    def _ambiguity_measure(self, m):
        """
        Compute the ambiguity measure.

        Parameters
        ----------
        m : ndarray of shape (n_samples, n_classes)
            Basic probability assignments associated to each class of the
            frame of discernment (i. e. the classes or target values).
            First column is assigned to the empty set; last column to the
            whole frame of discernment.
        
        Returns
        -------
        ndarray of shape (n_samples,)
            The ambiguity measure of each sample.
        """
        ambiguity = []
        
        for i in range(m.shape[0]):
            pignistic_probability = mtobetp(m[i])
            ambiguity.append(
                -np.sum(pignistic_probability * np.log2(pignistic_probability))
            )
        
        return np.array(ambiguity)


    def _bpa_to_matrix(self, X):
        """
        Return a matrix containing the mass functions of the 2^n elements of
        2^Theta.
        
        Returns
        -------
        m : ndarray of shape (n_samples, 2^n_classes)
            The mass functions on everything above.
        """
        bpa = self.classifier.predict(
            X, with_ignorance=False, _return="bpa", _distances=self.distances
        )
        m = np.zeros((bpa.shape[0], 2**bpa.shape[1]))

        for i in range(bpa.shape[1]):
            m[:, 2**i] = bpa[:, i]
        
        m[:, -1] = 1 - m[:, 0:-1].sum(axis=1)
        
        return m


    def _compute_distances(self):
        """
        Compute the distances between the pool and the training set and
        store them.
        """
        distances, distances_indices = self.classifier.predict(
            X=self.X[self.pool],
            _return="dist",
        )

        self.distances = (distances, distances_indices)

