#
#
#
#
#   Test of ./experiment/max_am.py
#
#
#
#

import unittest
import numpy as np
from scipy.sparse.construct import random

from sklearn.datasets import make_classification
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression

from modAL.models import ActiveLearner

from experiment.max_am import (
    ambiguity_measure,
    max_am,
    _bpa_to_matrix,
    _compute_bpa,
    _compute_other_bpa,
    _distances_among_labeled,
    _distances_among_all,
)


# -----------------------------------------------------------------------------
#   Test
# -----------------------------------------------------------------------------

class TestMaxAm(unittest.TestCase):
    """
    See Figure 2 from https://doi.org/10.1007/s11432-020-3082-9 to
    understand the samples adopted in the tests.
    """

    def test_ambiguity_measure(self):
        m = np.array([
            [.1146, .1839, .1839, .5175],
            [.0374, .6025, .0041, .3560],
            [.0911, .0420, .0002833, .8665],
            [.3739, .0175, .0175, .5853],
        ])

        am = ambiguity_measure(m)

        self.assertEqual(am.max(), am[3])
        self.assertGreater(am[0], am[1])
        self.assertGreater(am[2], am[1])

    def test_bpa_to_matrix(self):
        X_labeled = np.array((
            (-3, -1),
            (-4, 0),
            (-2, 0),
            (-3, 1),

            (3, -1),
            (2, 0),
            (3, 1),
            (4, 0),

            (100, 0),
            (102, 0),
            (101, -1),
            (101, 1),
        ))

        y_labeled = np.array([
            0, 0, 0, 0,
            1, 1, 1, 1,
            2, 2, 2, 2,
        ])

        X_unlabeled = np.array((
            (0, 0),
            (-5, 0),
            (-5, 7),
            (0, 7),
            (-5, 8),
        ))
        
        learner = LogisticRegression()
        learner.fit(X_labeled, y_labeled)

        bpa = _compute_bpa(X_unlabeled, X_labeled, learner.predict_proba)
        bpa_es, bpa_fod = _compute_other_bpa(X_unlabeled, X_labeled, bpa)

        m = _bpa_to_matrix(bpa, bpa_es, bpa_fod)

        self.assertTrue((m[:, 0] == bpa_es).all())
        self.assertTrue((m[:, -1] == bpa_fod).all())
        
        for sample in m:
            self.assertAlmostEqual(sample.sum(), 1.)

    def test_distances_among_labeled(self):
        samples = (
            (0, 0),
            (-5, 0),
            (-5, 7),
            (0, 7),
        )

        X_unlabeled = np.array(samples).reshape(-1, 2)
        X_labeled = np.array((
            (-3, -1),
            (-4, 0),
            (-2, 0),
            (-3, 1),

            (3, -1),
            (2, 0),
            (3, 1),
            (4, 0),
        )).reshape(-1, 2)
        
        smallest_distances = _distances_among_labeled(X_unlabeled, X_labeled)

        self.assertAlmostEqual(smallest_distances[0], 2.00000000)
        self.assertAlmostEqual(smallest_distances[1], 1.00000000)
        self.assertAlmostEqual(smallest_distances[2], 6.32455532)
        self.assertAlmostEqual(smallest_distances[3], 6.70820393)

    def test_distances_among_all(self):
        samples = (
            (0, 0),
            (-5, 0),
            (-5, 7),
            (0, 7),
        )

        X_unlabeled = np.array(samples).reshape(-1, 2)
        X_labeled = np.array((
            (-3, -1),
            (-4, 0),
            (-2, 0),
            (-3, 1),

            (3, -1),
            (2, 0),
            (3, 1),
            (4, 0),
        )).reshape(-1, 2)
        
        smallest_distances = _distances_among_all(X_unlabeled, X_labeled)

        self.assertAlmostEqual(smallest_distances[0], 2.)
        self.assertAlmostEqual(smallest_distances[1], 1.)
        self.assertAlmostEqual(smallest_distances[2], 5.)
        self.assertAlmostEqual(smallest_distances[3], 5.)

    def test_compute_bpas(self):
        samples = (
            (0, 0),
            (-5, 0),
            (-5, 7),
            (0, 7),
            (-5, 8),
        )

        def proba(x):
            return np.array([
                .5,    .5,
                .9933, .0067,
                .9933, .0067,
                .5,    .5,
                .9933, .0067,
            ]).reshape(-1, 2)
        
        X_unlabeled = np.array(samples).reshape(-1, 2)
        X_labeled = np.array((
            (-3, -1),
            (-4, 0),
            (-2, 0),
            (-3, 1),

            (3, -1),
            (2, 0),
            (3, 1),
            (4, 0),
        )).reshape(-1, 2)

        # BPA of singletons
        bpa = _compute_bpa(X_unlabeled, X_labeled, proba, a=.5)

        # BPA of empty set and frame of discernment
        # bpa_empt, bpa_fod = _compute_other_bpa(
        #     X_unlabeled, X_labeled, bpa, b=.5
        # )

        self.assertAlmostEqual(bpa[0, 0], 0.1839, places=4)
        self.assertAlmostEqual(bpa[0, 1], 0.1839, places=4)
        # self.assertAlmostEqual(bpa_empt[0], 0.1146, places=4)
        # self.assertAlmostEqual(bpa_fod[0],  0.5175, places=4)

        self.assertAlmostEqual(bpa[1, 0], 0.6025, places=4)
        self.assertAlmostEqual(bpa[1, 1], 0.0041, places=4)
        # self.assertAlmostEqual(bpa_empt[1], 0.0374, places=4)
        # self.assertAlmostEqual(bpa_fod[1],  0.3560, places=4)

        self.assertAlmostEqual(bpa[2, 0], 0.0420,    places=4)
        self.assertAlmostEqual(bpa[2, 1], 0.0002833, places=4)
        # self.assertAlmostEqual(bpa_empt[2], 0.0911, places=4)
        # self.assertAlmostEqual(bpa_fod[2],  0.8665, places=4)

        self.assertAlmostEqual(bpa[3, 0], 0.0175, places=4)
        self.assertAlmostEqual(bpa[3, 1], 0.0175, places=4)
        # self.assertAlmostEqual(bpa_empt[3], 0.3711, places=4)
        # self.assertAlmostEqual(bpa_fod[3],  0.5853, places=4)

    def test_max_am(self):
        X_labeled = np.array((
            (-3, -1),
            (-4, 0),
            (-2, 0),
            (-3, 1),

            (3, -1),
            (2, 0),
            (3, 1),
            (4, 0),
        ))

        y_labeled = np.array([
            0, 0, 0, 0,
            1, 1, 1, 1,
        ])

        X_unlabeled = np.array((
            # (3, 0),
            # (-3, 0),
            # (-1, -1),
            # (100, 100),

            (0, 0),
            (-5, 0),
            (-5, 7),
            (0, 7),
            (-5, 8),
        ))
        
        learner = ActiveLearner(
            estimator=LogisticRegression(),
            X_training=X_labeled,
            y_training=y_labeled,
        )

        max_am(learner, X_unlabeled, bpa_threshold=.5, n_instances=3)


# -----------------------------------------------------------------------------
#   Test
# -----------------------------------------------------------------------------

if __name__ == "__main__":
    unittest.main()
