"""
Unit tests for EKNN Sampling.
"""

import unittest
import numpy as np

from experiment.eknn_sampling import EKNNSampling


# ---------------------------------------------------------------------
#   EKNNSampling
# ---------------------------------------------------------------------

class TestEKNNSampling(unittest.TestCase):

    def test_bpa_to_matrix(self):
        """
        Tested methods
        --------------
        _bpa_to_matrix
        """
        X = np.array([
            [ 0,  0], [ 6, 6], [-10, -10],
            [-1,  0], [ 7, 7], [-11,  -9],
            [-3, -3], [15, 9], [-10, -12],
        ])

        y = np.array([
            0, 1, 2, 0, 1, 2, 0, 1, 2,
        ])

        train = [i for i in range(6)]
        pool  = [i for i in range(6, 9)]

        classifier = EKNNSampling(X, y, train, pool)
        
        m = classifier._bpa_to_matrix(X[pool])

        self.assertEqual(m.shape[1], 8)
        
        for i in range(m.shape[0]):
            self.assertAlmostEqual(1., m[i].sum())
        
        self.assertEqual(
            classifier.query(None, X[pool])[0],
            1
        )


    def test_compute_distances(self):
        """
        Tested methods
        --------------
        EKNNSampling.__init__
        EKNNSampling._compute_distances
        """
        X = np.array([
            [ 0,  0], [ 6, 6],
            [-1,  0], [ 7, 7],
            [-3, -3], [ 9, 9],
        ])

        y = np.array([
            0, 1, 0, 1, 0, 1,
        ])

        train = [0, 1, 2, 3,]
        pool  = [4, 5,]

        classifier = EKNNSampling(X, y, train, pool)

        self.assertListEqual(
            classifier.distances[0].tolist(),
            [[13], [8]],
        )

        self.assertListEqual(
            classifier.distances[1].tolist(),
            [[2], [3]],
        )


# ---------------------------------------------------------------------
#   Main
# ---------------------------------------------------------------------

if __name__ == "__main__":
    unittest.main()
